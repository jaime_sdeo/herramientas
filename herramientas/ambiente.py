import os
from django.core.exceptions import ImproperlyConfigured

def get_env_variable(var_name):
    try:
        return os.environ.get(var_name, None)
    except KeyError:
        error_msg = "Define previamente la variable %s" % var_name
        raise ImproperlyConfigured(error_msg)
